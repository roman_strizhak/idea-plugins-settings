##### Install plugin
1. Go to 'Preferences --> Plugins', press 'Browse Repositories' button.
2. In opened window in search field enter CheckStyle keyword, select CheckStyle-IDEA plugin and press 'Install plugin'.
3. Follow further standard plugin installation flow.

##### Set up
1. Go to 'Preferences --> Tools --> CheckStyle'.
2. Press 'Add' button in Configuration file section.
3. In dialog window set your Description, select 'Use local CheckStyle file' radio button.
4. Press 'Browse' and navigate to project repo directory with checkstyle.xml (checkstyle-import-control.xml and checkstyle-suppressions.xml should be in the same dir)
5. Select 'checkstyle.xml', click 'Next', choose property basedir and specify value for it with path ../idea-plugins-settings/ and press 'Next'.
6. Press "Finish" button, activate added rule and reload IDEA

CheckStyle influences to all open java files. Put mouse over the red text and read a prompt.

For more details see http://checkstyle.sourceforge.net/