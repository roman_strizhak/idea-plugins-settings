#### Automatically allow to rearrange methods and fields inside java classes.

##### Step-by-step guide
1. Open directory "\Users\${USER_NAME}\.IdeaIC2017.1\config\codestyles\"
2. Create the 'codestyles' directory
3. Copy file 'rearrangement.xml' to 'codestyle' directory
4. Go to "IntelliJ IDEA"
5. Navigate to: "File -> Settings -> Editor -> CodeStyle -> Java"
6. Select 'Arrangement' tab and click 'Manage' button
7. Select 'rearrangement' item and click 'OK'
9. Select any class file, which you want to rearrange
10. Navigate to: "Code -> Rearrange Code" and apply rearrange