#### Formatting Java classes

1. Install [Eclipse Code Formatter plugin](https://plugins.jetbrains.com/plugin/6546-eclipse-code-formatter)
2. Copy 'formatter.xml' and 'importorder.importorder' to some project directory
3. Open "Preferences | Eclipse Code Formatter"
4. Configure plugin:
    - Eclipse Java Formatter config file: {path_to_project}/ide_settings/formatter.xml
    - Import order -> From file: {path_to_project}/ide_settings/importorder.importorder

##### Other useful IDEA settings
1. Tabulation. "Editor -> Code Style" - turn off "Use tab character" option.
2. Grouping import. "Editor -> Code Style -> Java -> Imports Tab". Set 'Class/names count to use import with *' to 99