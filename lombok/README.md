##### Lombok info
You can find information about Lombok features by following these links:
- in English - https://projectlombok.org/features/index.html
- in Russian - (main features only) https://urvanov.ru/2015/09/22/project-lombok/

##### Plugin Installation
Link - https://github.com/mplushnikov/lombok-intellij-plugin

1. Using IDE built-in plugin system on Windows: File > Settings > Plugins > Browse repositories... > Search for "lombok" > Install Plugin
2. Restart IDE.
3. Click Settings-> "Build, Execution, Deployment" -> "Compiler, Annotation Processors". Click Enable Annotation Processing
4. Afterwards you might need to do a complete rebuild of your project via Build -> "Rebuild Project".

##### Lombok project dependency
Make sure you have Lombok dependency added to your project. This plugin does not automatically add it for you.

###### Maven
In your pom.xml:
```
<properties>
    <lombok.version>1.16.14</lombok.version>
</properties>
...
<dependencies>
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <version>${lombok.version}</version>
        <scope>provided</scope>
    </dependency>
</dependencies>
```